#include <iostream>
#include <GL/glew.h>
#include <glfw3.h>


#pragma comment (lib, "glfw3dll.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

#include <fstream>
#include <string>
#include <sstream>

#include "Renderer.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexArray.h"
#include "VertexBufferLayout.h"
#include "Shader.h"
#include "Texture.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "Camera.h"
#include "SkyBox.h"
#include "Mesh.h"
#include "Terrain.h"
#include "OBJLoader.h"

enum Movement
{
	Reset,
	Move,
	MoveBackwards,
	Pause
};
enum LightAction
{
	Sunrise,
	Sunset
};

enum CameraProperties
{
	FreeLook,
	TrainLook,
	InsideLook
};

glm::vec3 trainPos(-110.0f, 0.0f, 3.65f);

CameraProperties currentCamera = CameraProperties::FreeLook;


Mesh InitRailwayMesh(const Texture& texture)
{
	std::vector<float> vertices = {

		/*Vertex coords	           Texture coords               Normals*/
		 -18.0f,  -1.9f, 0.0f,		0.0f, 0.0f, 			0.0f,  1.0f, 0.0f,//0
		  18.0f,  -1.9f, 0.0f,		1.0f, 0.0f, 			0.0f,  1.0f, 0.0f,//1
		  18.0f,   1.9f, 0.0f,		1.0f, 1.0f, 			0.0f,  1.0f, 0.0f,//2
		 -18.0f,   1.9f, 0.0f,		0.0f, 1.0f,  			0.0f,  1.0f, 0.0f,//3
	};

	std::vector<unsigned int> indices = {
		0,1,2,
		2,3,0
	};

	return Mesh(vertices, indices, texture);
}
Mesh InitTerrainMesh(const Texture& texture)
{
	std::vector<float> vertices = {

		 18.0f,  0.8f,  4.0f,	0.0f, 0.0f,			0.0f,  1.0f, 0.0f,		//3 16
		 22.0f,  0.8f,  4.0f,	1.0f, 0.0f,			0.0f,  1.0f, 0.0f,		//2 17
		 22.0f,  0.8f,  0.0f,	1.0f, 1.0f,			0.0f,  1.0f, 0.0f,		//6 18
		 18.0f,  0.8f,  0.0f,	0.0f, 1.0f,			0.0f,  1.0f, 0.0f,		//5 19
	};

	std::vector<unsigned int> indices = {
		0,1,2,
		2,3,0
	};

	return Mesh(vertices, indices, texture);

}

float grassVertices[] = {
		0.5f, 0.5f,  0.0f,  1.0f, 0.0f,
		-0.5f, 0.5f,  0.0f,  0.0f, 0.0f,
		-0.5f, -0.5f, 0.0f,  0.0f, 1.0f,

		0.5f, 0.5f,  0.0f,  1.0f, 0.0f,
		-0.5f, -0.5f, 0.0f,  0.0f, 1.0f,
		0.5f, -0.5f, 0.0f,  1.0f, 1.0f,

		0.0f, 0.5f,  0.5f,  1.0f, 0.0f,
		0.0f, 0.5f,  -0.5f,  0.0f, 0.0f,
		0.0f, -0.5f, -0.5f,  0.0f, 1.0f,

		0.0f, 0.5f,  0.5f,  1.0f, 0.0f,
		0.0f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.0f, -0.5f, 0.5f,  1.0f, 1.0f
};

void RenderVegetation(Shader& object_shader, Camera& camera, Renderer& renderer,
	std::vector<std::pair<glm::vec3, int>>& terrainPos, Mesh& tree, Mesh& tree2, Mesh& terrainPatch)
{
	for (auto& pos : terrainPos)
	{
		if (std::abs(camera.GetPosition().x - pos.first.x - 15.0f) < 130.0f && std::abs(camera.GetPosition().z - pos.first.z - 15.0f) < 80.0f)
		{
			if (pos.second == 1)
			{
				glm::vec3 treePos = pos.first;
				treePos.y = -0.1f;
				treePos.x = treePos.x + 20.0f;

				if ((treePos.x < -70.f || treePos.x > -50.f) && (treePos.z > 20.f || treePos.z < 0.f)) // check vegetation before rendering because it may spawn in buildings
				{

					glm::mat4 model = glm::mat4(1.0f);
					model = glm::translate(model, treePos);
					model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));

					object_shader.Bind();
					object_shader.SetUniformMat4f("model", model);


					tree.Draw(camera, object_shader, renderer);
				}
			}

			else if (pos.second == 2)
			{
					glm::vec3 treePos = pos.first;
					treePos.y = -0.1f;
					treePos.x = treePos.x + 20.0f;
					if ((treePos.x < -70.f || treePos.x > -50.f) && (treePos.z > 20.f || treePos.z < 0.f))
					{
						glm::mat4 model = glm::mat4(1.0f);
						model = glm::translate(model, treePos);
						model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));

						object_shader.Bind();
						object_shader.SetUniformMat4f("model", model);

						tree2.Draw(camera, object_shader, renderer);
					}
			}

			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, pos.first);

			object_shader.Bind();
			object_shader.SetUniformMat4f("model", model);

			terrainPatch.Draw(camera, object_shader, renderer);
		}
	}

}

void RenderFirstStation(Shader& object_shader, Camera& camera, Renderer& renderer, Mesh& firstStation)
{

	if (std::abs(camera.GetPosition().x - 100.0f) < 130.0f && std::abs(camera.GetPosition().z - 10.0f) < 80.0f)
	{
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(100.0f, -1.4f, 10.0f));
		model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.7f, 0.7f, 0.7f));

		object_shader.Bind();
		object_shader.SetUniformMat4f("model", model);

		firstStation.Draw(camera, object_shader, renderer);
	}
}

void RenderSecondStation(Shader& object_shader, Camera& camera, Renderer& renderer, Mesh& secondStation)
{ 
	{
		if (std::abs(camera.GetPosition().x + 50.0f) < 130.0f && std::abs(camera.GetPosition().z - 10.0f) < 80.0f)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(-60.0f, 0.0f, 15.0f));

			object_shader.Bind();
			object_shader.SetUniformMat4f("model", model);

			secondStation.Draw(camera, object_shader, renderer);
		}
	}
}

void RenderRailway(Shader& object_shader, Camera& camera, Renderer& renderer,
	std::vector<glm::vec3>& railwayPos, Mesh& railway)
{
	//Rendering railway
	{
		for (auto& pos : railwayPos)
		{
			if (std::abs(camera.GetPosition().x - pos.x - 10.0f) < 130.0f && std::abs(camera.GetPosition().z - pos.z - 10.0f) < 80.0f)
			{
				glm::mat4 model = glm::mat4(1.0f);
				model = glm::translate(model, pos);
				model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));

				object_shader.Bind();
				object_shader.SetUniformMat4f("model", model);

				railway.Draw(camera, object_shader, renderer);
			}
		}
	}
}
unsigned int CreateTexture(const std::string& strTexturePath)
{
	unsigned int textureId = -1;

	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	//stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char* data = stbi_load(strTexturePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		std::cout << "Failed to load texture: " << strTexturePath << std::endl;
	}
	stbi_image_free(data);

	return textureId;
}

void RenderTrain(Shader& object_shader, Camera& camera, Renderer& renderer, Mesh& train, GLFWwindow* window,
	glm::vec3& lightPos, bool& day, Movement& move_train)
{
	glm::mat4 model = glm::mat4(0.7f);

	//train movement values
	double fIncrement = 0.0003;
	static double fMovementValue = 0.0;
	float current_x = glm::sin(fMovementValue) * 460.0f;
	lightPos.x = current_x;

	//train movement control keys
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		move_train = Movement::Pause;

	}
	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS)
	{
		move_train = Movement::MoveBackwards;
	}

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		move_train = Movement::Move;
	}

	if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
	{
		currentCamera = CameraProperties::TrainLook;
	}

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		currentCamera = CameraProperties::FreeLook;
	}

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		currentCamera = CameraProperties::InsideLook;
	}


	switch (move_train)
	{
	case Reset:
		break;
	case Move:
	{
		fMovementValue += fIncrement;
		model = glm::translate(model, glm::vec3(current_x, 0.0f, 0.0f));
		
		break;
	}
	case MoveBackwards:
	{
		fMovementValue -= fIncrement;
		model = glm::translate(model, glm::vec3(current_x, 0.0f, 0.0f));
		break;
	}
	case Pause:
	{
		model = glm::translate(model, glm::vec3(current_x, 0.0f, 0.0f));
		break;
	}
	default:
		break;
	};

	switch (currentCamera)
	{
	case FreeLook:
	{
		break;
	}
	case TrainLook:
	{
		camera.SetPosition(glm::vec3(current_x - 68.0f, 5.0f, 5.3f));
		break;
	}
	case InsideLook:
	{
		camera.SetPosition(glm::vec3(current_x - 65.5f, 3.0f, 5.3f));
		break;
	}
	default:
		break;
	};

	model = glm::translate(model, glm::vec3(-110.0f, 0.0f, 3.65f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));

	object_shader.Bind();
	object_shader.SetUniformMat4f("model", model);

	train.Draw(camera, object_shader, renderer);
}




int main(void)
{
	/* Initializing glfw */
	if (!glfwInit())
		return -1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Initializing sound engine */

	/* Creating a window */
	float window_height = 1200.0f, window_width = 2100.0f;
	GLFWwindow* window = glfwCreateWindow(window_width, window_height, "Train simulator", NULL, NULL);
	if (!window)
	{
		std::cout << "Failed to create window!\n";
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	/* Initializing glew */
	glewInit();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/* Loading shaders */
	Shader object_shader("object.shader");//shader used for all objects
	object_shader.Bind();
	object_shader.SetUniform4f("u_LightColor", 1.0f, 1.0f, 1.0f, 1.0f);

	Shader shadowMap_shader("shadowMap.shader"); //shaders used for shadows
	Shader shadowMapDepth_shader("shadowMapDepth.shader");

	/* Loading textures */
	Texture train_tex("../res/textures/thomas.png");
	Texture railway_tex("../res/textures/railway.jpg");
	Texture grass_plain("../res/textures/grass.jpg");
	Texture tree_1_tex("../res/textures/tree_1.png");
	Texture tree_2_tex("../res/textures/tree_2.png");
	Texture second_station_tex("../res/textures/first_station.png");
	Texture first_station_tex("../res/textures/second_station.png");

	unsigned int grassVAO, grassVBO;
	glGenVertexArrays(1, &grassVAO);
	glGenBuffers(1, &grassVBO);
	glBindVertexArray(grassVAO);
	glBindBuffer(GL_ARRAY_BUFFER, grassVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(grassVertices), &grassVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

	// Grass texture
	unsigned int grassTexture = CreateTexture("../res/textures/iarba.png");



	/* Initialize camera*/
	Camera camera(window_width, window_height, glm::vec3(9.0f, 10.0f, 33.0f));
	camera.SetPitch(-18.0f);
	float last_frame = 0.0f, delta_time;
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glfwSetCursorPos(window, (window_width / 2), (window_height / 2));


	Renderer renderer;

	glEnable(GL_DEPTH_TEST);

	Skybox skybox_scene;

	/* Loading models from.obj */
	std::vector<float> train_vertices;
	std::vector<unsigned int> train_indices;
	bool res = loadOBJ("../res/models/thomas.obj", train_vertices, train_indices);

	std::vector<float> tree_vertices;
	std::vector<unsigned int> tree_indices;
	bool res2 = loadOBJ("../res/models/tree_1.obj", tree_vertices, tree_indices);

	std::vector<float> tree2_vertices;
	std::vector<unsigned int> tree2_indices;
	bool res3 = loadOBJ("../res/models/tree_2.obj", tree2_vertices, tree2_indices);

	std::vector<float> first_station_vertices;
	std::vector<unsigned int> first_station_indices;
	bool res4 = loadOBJ("../res/models/first_station.obj", first_station_vertices, first_station_indices);

	std::vector<float> second_station_vertices;
	std::vector<unsigned int> second_station_indices;
	bool res5 = loadOBJ("../res/models/second_station.obj", second_station_vertices, second_station_indices);


	/* Loading meshes*/
	Mesh train(train_vertices, train_indices, train_tex);
	Mesh railway = InitRailwayMesh(railway_tex);
	Mesh terrainPatch = InitTerrainMesh(grass_plain);
	Mesh tree(tree_vertices, tree_indices, tree_1_tex);
	Mesh tree2(tree2_vertices, tree2_indices, tree_2_tex);
	Mesh first_station(first_station_vertices, first_station_indices, first_station_tex);
	Mesh second_station(second_station_vertices, second_station_indices, second_station_tex);

	/* Loading terrain & railway*/
	Terrain terrain;
	terrain.generatePositions(-100.0f, 100.0f, -50.0f, 50.0f);
	terrain.generateRailway(-60.0f, 110.0f);
	std::vector<std::pair<glm::vec3, int>>  terrainPos = terrain.getTerrainPositions();
	std::vector<glm::vec3> railwayPos = terrain.getRailway();

	Movement move_train = Movement::Reset;
	LightAction light_action = LightAction::Sunrise;

	float ambient_intensity = 0.7f;
	float diffuse = 0.2f;
	bool day = true;

	/* Setting shadows*/
	const unsigned int SHADOW_WIDTH = 2048, SHADOW_HEIGHT = 2048;
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);

	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	// Attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	shadowMap_shader.Bind();
	shadowMap_shader.SetUniform1i("diffuseTexture", 0);
	shadowMap_shader.SetUniform1i("shadowMap", 1);

	glm::vec3 lightPos(-10.f, 25.f, 10.0f);

	while (!glfwWindowShouldClose(window))
	{
		//Render here
		renderer.Clear();

		//Getting delta time
		float current_frame = static_cast<float>(glfwGetTime());
		delta_time = current_frame - last_frame;
		last_frame = current_frame;

		bool light_action_changed = false;

		//Setting light & sounds
		{
			shadowMap_shader.Bind();
			shadowMap_shader.SetUniform1f("u_AmbientIntensity", ambient_intensity);

			if (glfwGetKey(window, GLFW_KEY_L))
			{
				light_action_changed = true;
				light_action = LightAction::Sunrise;
				day = true;
			}

			if (glfwGetKey(window, GLFW_KEY_N))
			{
				light_action_changed = true;
				light_action = LightAction::Sunset;
				day = false;
			}

			switch (light_action)
			{
			case Sunrise:
			{
				if (ambient_intensity <= 0.7f)
				{
					ambient_intensity += 0.03f;
				}
				break;
			}
			case Sunset:
			{
				if (ambient_intensity > 0.3f)
				{
					ambient_intensity -= 0.03f;
				}
				break;
			}
			default:
				break;
			}
		}

		//Processing user input
		camera.ProcessInput(window, delta_time);

		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Render depth of scene to texture (from light's perspective)
		glm::mat4 lightProjection, lightView;
		glm::mat4 lightSpaceMatrix;
		float near_plane = -100.0f, far_plane = 200.5f;
		lightProjection = glm::ortho(-100.0f, 100.0f, -100.0f, 100.0f, near_plane, far_plane);
		lightView = glm::lookAt(lightPos + 20.f, lightPos - 20.f, glm::vec3(0.0, 1.0, 0.0));
		lightSpaceMatrix = lightProjection * lightView;

		shadowMapDepth_shader.Bind();
		shadowMapDepth_shader.SetUniformMat4f("lightSpaceMatrix", lightSpaceMatrix);

		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		//Rendering scene shadows
		RenderVegetation(shadowMapDepth_shader, camera, renderer, terrainPos, tree, tree2, terrainPatch);
		RenderRailway(shadowMapDepth_shader, camera, renderer, railwayPos, railway);
		RenderTrain(shadowMapDepth_shader, camera, renderer, train, window, lightPos, day, move_train);
		RenderFirstStation(shadowMapDepth_shader, camera, renderer, first_station);
		RenderSecondStation(shadowMapDepth_shader, camera, renderer, second_station);


		glCullFace(GL_BACK);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		//Reset viewport
		glViewport(0, 0, window_width, window_height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		shadowMap_shader.Bind();
		shadowMap_shader.SetUniformMat4f("projection", camera.GetProjectionMatrix());
		shadowMap_shader.SetUniformMat4f("view", camera.GetViewMatrix());

		//Set light uniforms
		shadowMap_shader.SetUniform3f("viewPos", camera.GetPosition().x, camera.GetPosition().y, camera.GetPosition().z);
		shadowMap_shader.SetUniform3f("lightPos", lightPos.x, lightPos.y, lightPos.z);
		shadowMap_shader.SetUniformMat4f("lightSpaceMatrix", lightSpaceMatrix);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		glDisable(GL_CULL_FACE);

		//Rendering scene
		RenderVegetation(shadowMap_shader, camera, renderer, terrainPos, tree, tree2, terrainPatch);
		RenderTrain(shadowMap_shader, camera, renderer, train, window, lightPos, day, move_train);
		RenderRailway(shadowMap_shader, camera, renderer, railwayPos, railway);
		RenderFirstStation(shadowMap_shader, camera, renderer, first_station);
		RenderSecondStation(shadowMap_shader, camera, renderer, second_station);

		// render grass
		glBindVertexArray(grassVAO);
		glBindTexture(GL_TEXTURE_2D, grassTexture);

		for (float x = -5.0f; x < 5.0f; x += 0.5f)
		{

			for (float y = -5.0f; y < 5.0f; y += 0.5f)
			{
				glm::mat4 model = glm::mat4();
				model = glm::translate(model, glm::vec3(x, 0, y));
				shadowMap_shader.Bind();
				shadowMap_shader.SetUniformMat4f("model", model);
				glDrawArrays(GL_TRIANGLES, 0, 12);
			}
		}

		//Rendering skybox
		if (light_action_changed)
		{
			skybox_scene.SetFaces(day);
		}
		skybox_scene.Draw(camera.GetViewMatrix(), camera.GetProjectionMatrix());



		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}
