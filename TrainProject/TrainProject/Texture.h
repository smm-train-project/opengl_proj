#pragma once

#include "Renderer.h"

class Texture
{
private:
	unsigned int m_renderID;	
	std::string m_filepath;
	unsigned char* m_localBuffer;
	int m_width, m_height, m_BPP;
public:
	Texture();
	Texture(unsigned int texture_id);
	Texture(const std::string& path);
	Texture(const Texture& texture);
	~Texture();

	void Bind(unsigned int slot = 0) const;
	void Unbind() const;

	inline int GetWidth() const { return m_width; }
	inline int GetHeight() const { return m_height; }
};

